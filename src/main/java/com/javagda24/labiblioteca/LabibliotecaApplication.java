package com.javagda24.labiblioteca;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class LabibliotecaApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(LabibliotecaApplication.class, args);
    }

}
